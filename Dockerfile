# Set the base image to node:12-alpine
FROM node:12-alpine as build

# Specify where our app will live in the container
WORKDIR /app

# Copy the React App to the container
COPY . /app/

# Prepare the container for building React
RUN npm install
RUN npm install pm2 -g

# Fire up nginx
EXPOSE 8080
STOPSIGNAL SIGQUIT
CMD ["pm2-runtime", "server.js"]
